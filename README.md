# Microscope resolution
Code for my PhD project on microscope resolution http://hdl.handle.net/21.11130/00-1735-0000-0005-1410-2.

## Scientific background

See `PhD_defense_slides_compressed.pdf` or https://arxiv.org/abs/2005.07450 for more details.

For simplicity, we just provide a short introduction here. As an example, we choose the 2D simulation as given in `simulation2D.cpp` and `sharedSimulationFunctions.h` files.
There 2D microscopy data are simulated as Gaussian, variance stabilized Gaussian,
Poissonian or binomial random variables. The goal is to figure
out the dependece of the resolution $`d`$ on the FWHM (full width at half maximum of the "signal"), on discretization $`n`$ and intensity $`t`$.

The PhD work considers the resolution in the statistical setting as a hypothesis testing problem
```math
        H_0: g(\vec{x}) = h(\vec{x}-\vec{x}_0)\\
        \text{vs}\\
        H_1: g(\vec{x}) = \frac{1}{2} h(\vec{x}-\vec{x}_1) + \frac{1}{2} h(\vec{x}-\vec{x}_2)
```
where $`h`$ is the point-spread-function (psf) -- property of the microscope in question -- and $`g`$ is the test function. So we are testing if we observe two peaks centered at $`\vec{x}_1`$ and $`\vec{x}_2`$, or one at $`\vec{x}_0`$. The smallest distance 
```math
d := |x_{11} - x_{21}|
```
such that we can distinguish $`H_0`$ from $`H_1`$ at a given level $`\alpha`$ and type II error $`\beta`$ is the **resolution** in the _first_ coordinate.

<figure>
    <img src="hypothesis.png"  width="342" height="250" align="middle">
    <img src="alternative.png"  width="342" height="250" align="middle"></pre>
    <figcaption> <i>Fig. 1. Statistical testing problem in 1D with Gaussian psf <i>h<\i> </figcaption>
</figure>

For simplicity we only describe the Poisson $`d`$ vs FWHM simulation. Others were done similarly. Looking through a microscope, at each detector grid point we observe photons. We model them by Poisson random variables $`Y_{ij}`$, $`1 \le i,j \le n `$, following
the distribution
```math
Y_{ij}\sim\mathrm{Poi} \left(t\int_{(i-1)/n}^{i/n} \int_{(j-1)/n}^{j/n} g(x,y)\, \mathrm{d}x\,\mathrm{d}y\right),
```
where $`g`$ is the test function defined above. As the psf $`h`$ we choose the Gaussian psf
```math
    h(x-a, y-b) = \frac{1}{2 \pi \sigma_1\sigma_2} \exp\left(-\frac{(x-a)^2}{2\sigma_1^2}\right)\exp\left(-\frac{(y-b)^2}{2\sigma_2^2}\right)
```
which is common in microscopy modeling. For the Gaussian psf it holds
```math
    \mathrm{FWHM} = 2\sqrt{2\log{2}}\,\sigma \approx 2.355\, \sigma.
```

We perform a 2D likelihood ratio test (optimal in this case) on the testing problem $`H_0\text{ vs }H_1`$ above with $`\vec{x}_0 = \frac{\vec{x}_1 + \vec{x}_2}{2}`$. Since we are interested in the first coordinate, we set $`x_{02} = x_{12} = x_{22} = 0.5`$. Since all the modelling is done in the unit interval, we furthermore fix $`x_{01} = 0.5`$.

We fix illumination time $`t`$, discretization $`n`$, test level $`\alpha`$ and type II error $`\beta`$. Then for each FWHM value in the range $`\{0.15, 0.16, 0.17, \ldots, 0.25\}`$ and $`d = d_{start}`$ we generate 1000 times the random variables $`Y_{ij}`$, $`1\le i,j \le n`$. We apply the 2D likelihood ratio test and check if
```math
0.95\,\beta \le \mathrm{type\; II\; error} < 1.05\,\beta.
```
If the above holds, then we interpret this $`d_{start}`$ as the microscope resolution $`d(t,n, \mathrm{FWHM}, \alpha, \beta)`$. Else,
if $`\mathrm{type\; II\; error} < 0.95\,\beta`$, we decrease $`d `$ and if $`\mathrm{type\; II\; error} \ge 1.05\,\beta`$ we increase $`d`$, and generate 1000 times the random variables $`Y_{ij}`$ again. We continue this process until the type II error is in the desired range. All in all, this gives us a set of points $`\{ (0.15, d_1), (0.16, d_2), \ldots, (0.25, d_{11})\}`$ which can be analyzed. In the Poisson model for $`t,n \ge 20`$ we get linear dependence of the resolution on FWHM, as expected by experiments. Furthermore, we find that our mathematically rigorous asymptotic theory ($`t,n\to\infty`$, $`\,d\to 0`$) can approximate simulations well if $`t\ge 500`$ and $`n\ge 50`$.

## How to run? / Description of files
To run the simulations in some particular dimension download the corresponding file `simulation1D.cpp`, `simulation2D.cpp` or `simulation3D.cpp` and the header file `sharedSimulationFunctions.h`. Then compile the files with the flags `-std=c++17 -lgsl -lgslcblas -lm -fopenmp -lstdc++fs`. This produces the corresponding programs to run. Currently, the simulations use four CPU cores. You can remove parallelization by deleting `#pragma omp parallel for` lines. Each of the simulations creates a separate folder to store the results. The Jupyter Notebook `simulation data analysis.ipynb` reads these data and analyzes them. First the data are converted to log-log scale then fitted and finally the fits are compared to our mathematically rigorous asymptotic theory. The plots produced by `simulation data analysis.ipynb` are saved in the `plots` folder.
