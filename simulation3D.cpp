#include <gsl/gsl_randist.h>
#include <gsl/gsl_cdf.h>

#include "sharedSimulationFunctions.h"

using namespace std;   // so that don't have to write std::cout etc.

////////////////////////////////////////////////////////////////////////////////
//// dummy 1D and 2D doTest functions for the header file //////////////////////
//// proper definitions in simulation1D.cpp and simulation2D.cpp respectively///
////////////////////////////////////////////////////////////////////////////////

vector<double>
doTest (string model, int t, int n, double dist, double sd1, int reps,
        double level, gsl_rng * seed, double q = 0.5) {

   vector<double> v { 0.0 };
   return v;
}

vector<double>
doTest (string model, int t, int n, double dist, double sd1, double sd2,
        int reps, double level, gsl_rng * seed, double q = 0.5) {

   vector<double> v { 0.0 };
   return v;
}

////////////////////////////////////////////////////////////////////////////////

// calculate probabilities for Gaussian kernel
pair < vector<vector<vector<double>>>, vector<vector<vector<double>>> >
calculateProbs (const int n = 50, double dist = 0.1,
                double sd1 = 0.08, double sd2 = 0.08, double sd3 = 0.08, double q = 0.5) {

   vector<vector<vector<double>>> probH(n, vector<vector<double>> ( n, vector<double>(n) ) );
   vector<vector<vector<double>>> probK(n, vector<vector<double>> ( n, vector<double>(n) ) );

   double x0[3] = { 0.5, 0.5, 0.5 };            // center the hypothesis Gaussian in the unit interval

   // put x1 and x2 at height 0.5
   double x1[3] = { 0.5 - (1-q) * dist, 0.5, 0.5 };
   double x2[3] = { 0.5 + q     * dist, 0.5, 0.5 };

   for (int i = 0; i < n; ++i) {
      for (int j = 0; j < n; ++j) {
         for (int k = 0; k < n; ++k) {

         // gaussian kernel case
         probH[i][j][k] =
            0.125 * ( erf( (i - n*x0[0]) / (sqrt(2)*n*sd1) ) - erf( (i - 1 - n*x0[0]) / (sqrt(2)*n*sd1) ) )
                  * ( erf( (j - n*x0[1]) / (sqrt(2)*n*sd2) ) - erf( (j - 1 - n*x0[1]) / (sqrt(2)*n*sd2) ) )
                  * ( erf( (k - n*x0[2]) / (sqrt(2)*n*sd3) ) - erf( (k - 1 - n*x0[2]) / (sqrt(2)*n*sd3) ) );

         probK[i][j][k] =
            0.125*(
                 q*
                 ( erf( (i - n*x1[0]) / (sqrt(2)*n*sd1) ) - erf( (i - 1 - n*x1[0]) / (sqrt(2)*n*sd1) ) )
               * ( erf( (j - n*x1[1]) / (sqrt(2)*n*sd2) ) - erf( (j - 1 - n*x1[1]) / (sqrt(2)*n*sd2) ) )
               * ( erf( (k - n*x1[2]) / (sqrt(2)*n*sd3) ) - erf( (k - 1 - n*x1[2]) / (sqrt(2)*n*sd3) ) )

               + (1-q)*
                 ( erf( (i - n*x2[0]) / (sqrt(2)*n*sd1) ) - erf( (i - 1 - n*x2[0]) / (sqrt(2)*n*sd1) ) )
               * ( erf( (j - n*x2[1]) / (sqrt(2)*n*sd2) ) - erf( (j - 1 - n*x2[1]) / (sqrt(2)*n*sd2) ) )
               * ( erf( (k - n*x2[2]) / (sqrt(2)*n*sd3) ) - erf( (k - 1 - n*x2[2]) / (sqrt(2)*n*sd3) ) )
                     );
         }
      }
   }
   return make_pair(probH, probK);
}

pair < vector<vector<vector<double>>>, vector<vector<vector<double>>> >
generateData (string model, int t, int n, double dist,
              pair < vector<vector<vector<double>>>, vector<vector<vector<double>>> >
                  probs,
              gsl_rng * seed) {

   gsl_rng * r = seed;

   vector<vector<vector<double>>> Y0(n, vector<vector<double>> ( n, vector<double>(n) ) );
   vector<vector<vector<double>>> Y1(n, vector<vector<double>> ( n, vector<double>(n) ) );

   if (model == "gauss") {
      for (int i = 0; i < n; ++i) {
         for (int j = 0; j < n; ++j) {
            for (int k = 0; k < n; ++k) {
               Y0[i][j][k] = gsl_ran_gaussian(r, 1) + t*probs.first [i][j][k];
               Y1[i][j][k] = gsl_ran_gaussian(r, 1) + t*probs.second[i][j][k];
            }
         }
      }
   } else if (model == "gaussEquivPoi") {
      for (int i = 0; i < n; ++i) {
         for (int j = 0; j < n; ++j) {
            for (int k = 0; k < n; ++k) {
               Y0[i][j][k] = gsl_ran_gaussian(r, 1) + 2*sqrt(t*probs.first [i][j][k]);
               Y1[i][j][k] = gsl_ran_gaussian(r, 1) + 2*sqrt(t*probs.second[i][j][k]);
            }
         }
      }
   // should have integer containers
   } else if (model == "poisson") {
      for (int i = 0; i < n; ++i) {
         for (int j = 0; j < n; ++j) {
            for (int k = 0; k < n; ++k) {
               Y0[i][j][k] = gsl_ran_poisson(r, t*probs.first [i][j][k]);
               Y1[i][j][k] = gsl_ran_poisson(r, t*probs.second[i][j][k]);
            }
         }
      }
   } else {
      for (int i = 0; i < n; ++i) {
         for (int j = 0; j < n; ++j) {
            for (int k = 0; k < n; ++k) {
               Y0[i][j][k] = gsl_ran_binomial(r, probs.first [i][j][k], t);
               Y1[i][j][k] = gsl_ran_binomial(r, probs.second[i][j][k], t);
            }
         }
      }
   }
   return make_pair(Y0, Y1);
}

////////////////////////////////////////////////////////////////////////////////
//////////////////// Different quantile functions //////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// // checks bounds
// double checkBounds(double x, double bound = 1e100, bool positive = false) {
//    if ( positive == false ) { if (x > bound || x < -bound) { x = 0.0; } }
//    else                     { if (x > bound || x < 0)      { x = 0.0; } }
//    return x;
// }

double
quantilePoi (int t, int n,
              pair < vector<vector<vector<double>>>, vector<vector<vector<double>>> >
                     probs,
              double level) {

//   double E0temp, V0temp;

   double expE0 = 1.0;
   double expV0 = 1.0;

   for (int i = 0; i < n; ++i) {
      for (int j = 0; j < n; ++j) {
         for (int k = 0; k < n; ++k) {

            double probH = probs.first [i][j][k];
            double probK = probs.second[i][j][k];

            // if (probH > 1e-10) {
            expE0 *= pow( probK / probH, t * probH);
            expV0 *= pow( probK / probH, log( pow( probK / probH, t * probH ) ) );
            // }

   //         E0 += checkBounds(E0temp);
   //         V0 += checkBounds(V0temp, true);
         }
      }
   }

   double
   qTheory = exp(gsl_cdf_ugaussian_Pinv(1.0-level)*sqrt(log(expV0))) * expE0;

   return qTheory;
}

// deterministic
double
quantileGauss (int t, int n,
              pair < vector<vector<vector<double>>>, vector<vector<vector<double>>> >
                     probs,
               double level) {

   double s = 0.0;

   for (int i = 0; i < n; ++i) {
      for (int j = 0; j < n; ++j) {
         for (int k = 0; k < n; ++k) {
            s += pow(probs.second[i][j][k]-probs.first[i][j][k], 2);
         }
      }
   }

   double mu = pow(t, 2)/ 2.0  * s;

   // if exponentiated, i.e. exp( ... ), then accuracy decreases
   // i.e. cannot distinguish t = 10 000 from t = 1000 000 because returns 0
   // so this is more accurate
   double quantile;
   quantile = sqrt(2.0*mu)*gsl_cdf_ugaussian_Pinv(1.0-level) - mu;

   return quantile;
}

// deterministic
double
quantileGaussEquivPoi (int t, int n,
                     pair < vector<vector<vector<double>>>, vector<vector<vector<double>>> >
                           probs,
                      double level) {

   double s = 0.0;

   for (int i = 0; i < n; ++i) {
      for (int j = 0; j < n; ++j) {
         for (int k = 0; k < n; ++k) {
            s += pow( sqrt( probs.second[i][j][k] ) - sqrt( probs.first[i][j][k] ) , 2);
         }
      }
   }

   double nu = 2*t*s;

   // if exponentiated, i.e. exp( ... ), then accuracy decreases
   // i.e. cannot distinguish t = 10 000 from t = 1000 000 because returns 0
   // so this is more accurate
   double quantile;
   quantile = sqrt( 2.0*nu ) * gsl_cdf_ugaussian_Pinv(1.0-level) - nu;

   return quantile;
}

double calculateQuantile (string model, int t, int n,
                     pair < vector<vector<vector<double>>>, vector<vector<vector<double>>> >
                           probs,
                      double level) {

   double quantile;

   if (model == "gauss") {
      quantile = quantileGauss(t, n, probs, level);
   } else if (model == "gaussEquivPoi") {
      quantile = quantileGaussEquivPoi(t, n, probs, level);
   } else {
      quantile = quantilePoi(t, n, probs, level);
   }

   return quantile;
}

pair <double, double>
getTestStats (string model, int t, int n, double dist,
              pair < vector<vector<vector<double>>>, vector<vector<vector<double>>> >
                        probs,
              pair < vector<vector<vector<double>>>, vector<vector<vector<double>>> >
                        data,
               double level) {

   // T columns and S rows
   double T_H;
   double T_K;

   double s = 0.0;

   if (model == "gauss") {
      for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
               for (int k = 0; k < n; ++k) {

                  T_H += 2 * data.first [i][j][k] * t * ( probs.second[i][j][k]-probs.first[i][j][k] );
                  T_K += 2 * data.second[i][j][k] * t * ( probs.second[i][j][k]-probs.first[i][j][k] );

                  s += pow(t*probs.first[i][j][k], 2)-pow(t*probs.second[i][j][k], 2);
               }
            }
      }
      T_H += s;
      T_H *= 1.0 / 2.0;

      T_K += s;
      T_K *= 1.0 / 2.0;

   } else if (model == "gaussEquivPoi") {
      T_H = 0.0;
      T_K = 0.0;

      for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
               for (int k = 0; k < n; ++k) {

                  T_H += 2 * data.first [i][j][k] * sqrt(t) *
                        ( sqrt( probs.second[i][j][k] ) - sqrt( probs.first[i][j][k] ) );

                  T_K += 2 * data.second[i][j][k] * sqrt(t) *
                        ( sqrt( probs.second[i][j][k] ) - sqrt( probs.first[i][j][k] ) );

                  s += 2*t*(probs.first[i][j][k] - probs.second[i][j][k]);
               }
            }
         }
         T_H += s;
         T_K += s;

   } else if (model == "poisson") {
      T_H = 1.0;
      T_K = 1.0;

      for (int i = 0; i < n; ++i) {
         for (int j = 0; j < n; ++j) {
            for (int k = 0; k < n; ++k) {

               double probH = probs.first [i][j][k];
               double probK = probs.second[i][j][k];

               // if (probH > 1e-100) {
               T_H *= pow(probK / probH, data.first [i][j][k]);
               T_K *= pow(probK / probH, data.second[i][j][k]);
               // }
            }
         }
      }

   } else {
      T_H = 1.0;
      T_K = 1.0;

      for (int i = 0; i < n; ++i) {
         for (int j = 0; j < n; ++j) {
            for (int k = 0; k < n; ++k) {

               double probH = probs.first [i][j][k];
               double probK = probs.second[i][j][k];

               T_H *= pow( probK * (1-probH) / ( probH * (1-probK) ), data.first [i][j][k] );
               T_K *= pow( probK * (1-probH) / ( probH * (1-probK) ), data.second[i][j][k] );
               // }
            }
         }
      }
   }
   return make_pair(T_H, T_K);
}

vector<double>
doTest (string model, int t, int n, double dist, double sd1, double sd2, double sd3, int reps,
        double level, gsl_rng * seed, double q = 0.5) {

   pair < vector<vector<vector<double>>>, vector<vector<vector<double>>> >
   probs = calculateProbs (n, dist, sd1, sd2, sd3, q);

   double quantile = calculateQuantile (model, t, n, probs, level);

   int false_H = 0, false_K = 0;

   pair < vector<vector<vector<double>>>, vector<vector<vector<double>>> >
   data;

   for (int rep = 0; rep < reps; ++rep) {

      pair < vector<vector<vector<double>>>, vector<vector<vector<double>>> >
      data = generateData (model, t, n, dist, probs, seed);

      pair<double, double>
      stats = getTestStats (model, t, n, dist, probs, data, level);

      if (stats.first  >  quantile ) { ++false_H; }
      if (stats.second <= quantile ) { ++false_K; }
   }

   double error1 = 1.0*false_H/reps;
   double error2 = 1.0*false_K/reps;

   vector<double> v { error1, error2 };
   return v;
}

// CIA CIA CIA
int main() {
   ///////////// set up the random environment
   gsl_rng * seed;
   const gsl_rng_type * T;

   gsl_rng_env_setup();
   T = gsl_rng_default;
   seed = gsl_rng_alloc (T);
   /////////////////////////////

   ///////////// set other parameter values used for both simulations
   vector<string> models = {"gauss", "gaussEquivPoi", "poisson", "binomial"};

   int reps = 1e3;
   double level = 0.1;

   int seedStart1 = 6767;
   int seedStart2 = 13;


   vector<int> ns, times;
   vector<double> fwhms;

   string fileEnding;
  ///////////////////////////////////////////////////////////////////
  //
  // SMALL PARAMETER VALUES
  //
  ////////////////////////////////////////////////////////////////////

  // parameter values for FWHM vs d
  int t, n;
  double fwhm;

  t = 500;
  n = 50;

  ns    = { n };
  times = { t };
  fwhms = parameterGrid (0.15, 0.25);

  string whichCoordinate = "first";
  fileEnding = createFileName(t, fwhm, n, seedStart1, "fwhm", whichCoordinate);

  //simulate FWHM vs d
  #pragma omp parallel for
  for (vector<string>::size_type i = 0; i < models.size(); i++) {
   gsl_rng_set(seed, seedStart1); // should make the variable private
   findDetectionBoundary (models[i], fwhms, times, ns,
                    reps, level, seed, whichCoordinate, fileEnding, "3D");
  }


  whichCoordinate = "second";
  fileEnding = createFileName(t, fwhm, n, seedStart1, "fwhm", whichCoordinate);

  // simulate FWHM vs d
  #pragma omp parallel for
  for (vector<string>::size_type i = 0; i < models.size(); i++) {
   gsl_rng_set(seed, seedStart1); // should make the variable private
   findDetectionBoundary (models[i], fwhms, times, ns,
                    reps, level, seed, whichCoordinate, fileEnding, "3D");
  }

  whichCoordinate = "third";
  fileEnding = createFileName(t, fwhm, n, seedStart1, "fwhm", whichCoordinate);

  // simulate FWHM vs d
  #pragma omp parallel for
  for (vector<string>::size_type i = 0; i < models.size(); i++) {
    gsl_rng_set(seed, seedStart1); // should make the variable private
    findDetectionBoundary (models[i], fwhms, times, ns,
                     reps, level, seed, whichCoordinate, fileEnding, "3D");
  }

  whichCoordinate = "all";
  fileEnding = createFileName(t, fwhm, n, seedStart1, "fwhm", whichCoordinate);

  // simulate FWHM vs d
  #pragma omp parallel for
  for (vector<string>::size_type i = 0; i < models.size(); i++) {
    gsl_rng_set(seed, seedStart1); // should make the variable private
    findDetectionBoundary (models[i], fwhms, times, ns,
                     reps, level, seed, whichCoordinate, fileEnding, "3D");
  }
  //////////////////////////////////////////////////////////////////////////////




   gsl_rng_free(seed);

   // make a beep when finished
   cout << '\a';
}
