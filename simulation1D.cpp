#include <gsl/gsl_randist.h>
#include <gsl/gsl_cdf.h>

#include "sharedSimulationFunctions.h"

using namespace std;   // so that don't have to write std::cout etc.

////////////////////////////////////////////////////////////////////////////////
//// dummy 2D and 3D doTest functions for the header file //////////////////////
//// proper definitions in simulation2D.cpp and simulation3D.cpp respectively///
////////////////////////////////////////////////////////////////////////////////

vector<double>
doTest (string model, int t, int n, double dist, double sd1, double sd2, int reps,
        double level, gsl_rng * seed, double q = 0.5) {

   vector<double> v { 0.0 };
   return v;
}

vector<double>
doTest (string model, int t, int n, double dist, double sd1, double sd2, double sd3,
        int reps, double level, gsl_rng * seed, double q = 0.5) {

   vector<double> v { 0.0 };
   return v;
}

////////////////////////////////////////////////////////////////////////////////

// calculate probabilities for Gaussian kernel
pair < vector<double>, vector<double> >
calculateProbs (int n = 100, double dist = 0.1, double sd = 0.08, double q = 0.5) {

   vector<double> probsH (n, 0);
   vector<double> probsK (n, 0);


   double x0 = 0.5;    // center the Gaussian psf at the center of the interval

   // put x1 and x2 distance dist apart
   double x1 = x0 - (1-q) * dist;
   double x2 = x0 + q     * dist;

   for (int i = 0; i < n; ++i) {
         // gaussian case
         probsH[i] =
            0.5  * ( erf( (i - n*x0) / (sqrt(2)*n*sd) ) - erf( (-1 + i - n*x0)/(sqrt(2)*n*sd) ) );

         probsK[i] =
            0.5 * (
                     q*( erf( (i - n*x1) / (sqrt(2)*n*sd) ) - erf( (-1 + i - n*x1)/(sqrt(2)*n*sd) ) )
               +
                 (1-q)*( erf( (i - n*x2) / (sqrt(2)*n*sd) ) - erf( (-1 + i - n*x2)/(sqrt(2)*n*sd) ) )
                     );
   }
   return make_pair(probsH, probsK);
}


vector<double>
doTest (string model, int t, int n, double dist, double sd, int reps,
        double level, gsl_rng * seed, double q) {

   pair < vector<double>, vector<double> >
   probs = calculateProbs (n, dist, sd, q);

   double quantile = calculateQuantile (model, t, n, probs, level);

   int false_H = 0, false_K = 0;

   pair < vector<double>, vector<double> > data;

   for (int rep = 0; rep < reps; ++rep) {

      pair < vector<double>, vector<double> >
      data = generateData (model, t, n, dist, probs, seed);

      pair<double, double>
      stats = getTestStats (model, t, n, dist, probs, data, level);

      if (stats.first  >  quantile ) { ++false_H; }
      if (stats.second <= quantile ) { ++false_K; }
   }

   double error1 = 1.0*false_H/reps;
   double error2 = 1.0*false_K/reps;

   vector<double> v { error1, error2 };
   return v;
}


pair < vector<double>, vector<double> >
generateData (string model, int t, int n, double dist,
              pair < vector<double>, vector<double> >
                  probs,
              gsl_rng * seed) {

   gsl_rng * r = seed;

   vector<double> Y0( n, 0 );
   vector<double> Y1( n, 0 );

   if (model == "gauss") {
      for (int i = 0; i < n; ++i) {
         Y0[i] = gsl_ran_gaussian(r, 1) + t*probs.first [i];
         Y1[i] = gsl_ran_gaussian(r, 1) + t*probs.second[i];
      }
   } else if (model == "gaussEquivPoi") {
      for (int i = 0; i < n; ++i) {
         Y0[i] = gsl_ran_gaussian(r, 1) + 2*sqrt(t*probs.first [i]);
         Y1[i] = gsl_ran_gaussian(r, 1) + 2*sqrt(t*probs.second[i]);
      }
   // should have integer containers
   } else if (model == "poisson") {
      for (int i = 0; i < n; ++i) {
         Y0[i] = gsl_ran_poisson(r, t*probs.first [i]);
         Y1[i] = gsl_ran_poisson(r, t*probs.second[i]);
      }
   } else {
      for (int i = 0; i < n; ++i) {
         Y0[i] = gsl_ran_binomial(r, probs.first [i], t);
         Y1[i] = gsl_ran_binomial(r, probs.second[i], t);
      }
   }

   return make_pair(Y0, Y1);
}


////////////////////////////////////////////////////////////////////////////////
//////////////////// Different quantile functions //////////////////////////////
////////////////////////////////////////////////////////////////////////////////
double
quantilePoi (int t, int n,
              pair < vector<double>, vector<double> >
                     probs,
              double level) {

   double E0 = 0.0;
   double V0 = 0.0;

   for (int i = 0; i < n; ++i){

      double probH = probs.first[i];
      double probK = probs.second[i];

      E0 +=     log(probK / probH)       * t * probH;
      V0 += pow(log(probK / probH), 2.0) * t * probH;

   }

   double
   qTheory = gsl_cdf_ugaussian_Pinv(1.0-level)*sqrt(V0) + E0;

   return qTheory;
}

// deterministic
double
quantileGauss (int t, int n,
               pair < vector<double>, vector<double> >
                     probs,
               double level) {

   double s = 0.0;

   for (int i = 0; i < n; ++i) {
      s = s + pow(probs.second[i]-probs.first[i], 2);
   }

   double mu = pow(t, 2)/ 2.0  * s;

   double quantile;
   quantile = sqrt(2.0*mu)*gsl_cdf_ugaussian_Pinv(1.0-level) - mu;

   return quantile;
}

// deterministic
double
quantileGaussEquivPoi (int t, int n,
                      pair < vector<double>, vector<double> >
                           probs,
                      double level) {

   double s = 0.0;

   for (int i = 0; i < n; ++i) {
      s = s + pow( sqrt( probs.second[i] ) - sqrt( probs.first[i] ) , 2);
   }

   double nu = 2*t*s;

   double quantile;
   quantile = sqrt( 2.0*nu ) * gsl_cdf_ugaussian_Pinv(1.0-level) - nu;

   return quantile;
}

double calculateQuantile (string model, int t, int n,
                      pair < vector<double>, vector<double> >
                           probs,
                      double level) {

   double quantile;

   if (model == "gauss") {
      quantile = quantileGauss(t, n, probs, level);
   } else if (model == "gaussEquivPoi") {
      quantile = quantileGaussEquivPoi(t, n, probs, level);
   } else {
      quantile = quantilePoi(t, n, probs, level);
   }

   return quantile;
}
////////////////////////////////////////////////////////////////////////////////


pair <double, double>
getTestStats (string model, int t, int n, double dist,
               pair < vector<double>, vector<double> >
                        probs,
               pair < vector<double>, vector<double> >
                        data,
               double level) {

   // T_H is test statistic under the hypothesis H_0
   // T_K is test statistic under the alternative H_1
   double T_H = 0.0;
   double T_K = 0.0;

   double s = 0.0;

   if (model == "gauss") {
      for (int i = 0; i < n; ++i) {
         T_H += 2 * data.first [i] * t * (probs.second[i]-probs.first[i]);
         T_K += 2 * data.second[i] * t * (probs.second[i]-probs.first[i]);

         s = s + pow(t*probs.first[i], 2)-pow(t*probs.second[i], 2);
      }
      T_H += s;
      T_H = 1.0 / 2.0 * T_H;

      T_K += s;
      T_K = 1.0 / 2.0 *T_K;

   } else if (model == "gaussEquivPoi") {
      for (int i = 0; i < n; ++i) {
         T_H += 2 * data.first [i] * sqrt(t) * ( sqrt( probs.second[i] ) - sqrt( probs.first[i] ) );
         T_K += 2 * data.second[i] * sqrt(t) * ( sqrt( probs.second[i] ) - sqrt( probs.first[i] ) );

         s = s + 2*t*(probs.first[i] - probs.second[i]);
      }
         T_H += s;
         T_K += s;

   } else if (model == "poisson") {
      for (int i = 0; i < n; ++i) {

         double probH = probs.first [i];
         double probK = probs.second[i];

         T_H += log(probK / probH) * data.first [i];
         T_K += log(probK / probH) * data.second[i];
      }

   } else {
      for (int i = 0; i < n; ++i) {
         double probH = probs.first [i];
         double probK = probs.second[i];
         double weight = log(probK * (1-probH) / (probH * (1-probK)) );

         T_H += weight * data.first [i];
         T_K += weight * data.second[i];
      }
   }

   return make_pair(T_H, T_K);
}


int main() {
  ///////////// set up the random environment
  gsl_rng * seed;
  const gsl_rng_type * T;

  gsl_rng_env_setup();
  T = gsl_rng_default;
  seed = gsl_rng_alloc (T);
  /////////////////////////////

  ////////// set parameter values used for both simulations //////////////
  vector<string> models = {"gauss", "gaussEquivPoi", "poisson", "binomial"};

  int reps = 1e4;
  double level = 0.1;

  int seedStart1 = 6767;
  int seedStart2 = 13;

  vector<int> ns, times;
  vector<double> fwhms;

  string fileEnding;
  ///////////////////////////////////////////////////////////////////
  //
  // SMALL PARAMETER VALUES
  //
  ////////////////////////////////////////////////////////////////////

  // parameter values for FWHM vs d
  gsl_rng_set(seed, seedStart1);

  int t, n;
  double fwhm = 0.2;

  t = 20;
  n = 20;

  ns    = { n };
  times = { t };
  fwhms = parameterGrid (0.15, 0.25);

  fileEnding = createFileName(t, fwhm, n, seedStart1, "fwhm", "");

  // simulate FWHM vs d
  #pragma omp parallel for
  for (vector<string>::size_type i = 0; i < models.size(); i++) {
    findDetectionBoundary (models[i], fwhms, times, ns,
      reps, level, seed, "", fileEnding, "1D");
  }
  //////////////////////////////////////////////////////////////////

  // parameter values for t vs d
  gsl_rng_set(seed, seedStart1);

  n = 20;
  fwhm = 0.2;

  ns = { n };
  times = parameterGridInt (10, 20, 1);
  fwhms = { fwhm };

  fileEnding = createFileName(t, fwhm, n, seedStart1, "t", "");

  // simulate t vs d
  #pragma omp parallel for
  for (vector<string>::size_type i = 0; i < models.size(); i++) {
     findDetectionBoundary (models[i], fwhms, times, ns,
                      reps, level, seed, "", fileEnding, "1D");
  }
  ////////////////////////////////////////////////////////////////////

  // parameter values for n vs d
  gsl_rng_set(seed, seedStart1);

  t = 20;
  fwhm = 0.2;

  ns = parameterGridInt (50, 100, 10);
  times = { t };
  fwhms = { fwhm };

  fileEnding = createFileName(t, fwhm, n, seedStart1, "n", "");

  // simulate n vs d
  #pragma omp parallel for
  for (vector<string>::size_type i = 0; i < models.size(); i++) {
     findDetectionBoundary (models[i], fwhms, times, ns,
                      reps, level, seed, "", fileEnding, "1D");
  }

  ///////////////////////////////////////////////////////////////////
  //
  // LARGE PARAMETER VALUES
  //
  ////////////////////////////////////////////////////////////////////

  // parameter values for FWHM vs d
  gsl_rng_set(seed, seedStart1);

  fwhm = 0.2;

  t = 500;
  n = 500;

  ns    = { n };
  times = { t };
  fwhms = parameterGrid (0.15, 0.25);

  fileEnding = createFileName(t, fwhm, n, seedStart1, "fwhm", "");

  // simulate FWHM vs d
  #pragma omp parallel for
  for (vector<string>::size_type i = 0; i < models.size(); i++) {
     findDetectionBoundary (models[i], fwhms, times, ns,
                      reps, level, seed, "", fileEnding, "1D");
  }
  ////////////////////////////////////////////////////////////////////

  // parameter values for t vs d
  gsl_rng_set(seed, seedStart1);

  n = 500;
  fwhm = 0.2;

  ns = { n };
  times = parameterGridInt (50, 500, 50);
  fwhms = { fwhm };

  fileEnding = createFileName(t, fwhm, n, seedStart1, "t", "");

  // simulate t vs d
  #pragma omp parallel for
  for (vector<string>::size_type i = 0; i < models.size(); i++) {
     findDetectionBoundary (models[i], fwhms, times, ns,
                      reps, level, seed, "", fileEnding, "1D");
  }
  ////////////////////////////////////////////////////////////////////

  // parameter values for n vs d
  gsl_rng_set(seed, seedStart1);

  t = 500;
  fwhm = 0.2;

  ns = parameterGridInt (50, 500, 50);
  times = { t };
  fwhms = { fwhm };

  fileEnding = createFileName(t, fwhm, n, seedStart1, "n", "");

  // simulate n vs d
  #pragma omp parallel for
  for (vector<string>::size_type i = 0; i < models.size(); i++) {
     findDetectionBoundary (models[i], fwhms, times, ns,
                      reps, level, seed, "", fileEnding, "1D");
  }

  //*****************************************************************
  ///////////////////////////////////////////////////////////////////
  // ASYMMETRIC CASE
  // x0 = q * x1 + (1-q) * x2
  //////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////

  ///////////////////////////////////////////////////////////////////
  //
  // SMALL PARAMETER VALUES
  //
  ////////////////////////////////////////////////////////////////////

  double q = 0.2;

  // parameter values for FWHM vs d
  gsl_rng_set(seed, seedStart1);

  fwhm = 0.2;

  t = 20;
  n = 20;

  ns    = { n };
  times = { t };
  fwhms = parameterGrid (0.15, 0.25);

  fileEnding = createFileName(t, fwhm, n, seedStart1, "fwhm", "", q);

  // simulate FWHM vs d
  #pragma omp parallel for
  for (vector<string>::size_type i = 0; i < models.size(); i++) {
     findDetectionBoundary (models[i], fwhms, times, ns,
                      reps, level, seed, "", fileEnding, "1D", q);
  }
  ////////////////////////////////////////////////////////////////////

  // parameter values for t vs d
  gsl_rng_set(seed, seedStart1);

  n = 20;
  fwhm = 0.2;

  ns = { n };
  times = parameterGridInt (10, 20, 1);
  fwhms = { fwhm };

  fileEnding = createFileName(t, fwhm, n, seedStart1, "t", "", q);

  // simulate t vs d
  #pragma omp parallel for
  for (vector<string>::size_type i = 0; i < models.size(); i++) {
     findDetectionBoundary (models[i], fwhms, times, ns,
                      reps, level, seed, "", fileEnding, "1D", q);
  }
  ////////////////////////////////////////////////////////////////////

  // parameter values for n vs d
  gsl_rng_set(seed, seedStart1);

  t = 20;
  fwhm = 0.2;

  ns = parameterGridInt (50, 100, 10);
  times = { t };
  fwhms = { fwhm };

  fileEnding = createFileName(t, fwhm, n, seedStart1, "n", "", q);

  // simulate n vs d
  #pragma omp parallel for
  for (vector<string>::size_type i = 0; i < models.size(); i++) {
     findDetectionBoundary (models[i], fwhms, times, ns,
                      reps, level, seed, "", fileEnding, "1D", q);
  }

  ///////////////////////////////////////////////////////////////////
  //
  // LARGE PARAMETER VALUES
  //
  ////////////////////////////////////////////////////////////////////

  // parameter values for FWHM vs d
  gsl_rng_set(seed, seedStart1);

  fwhm = 0.2;

  t = 500;
  n = 500;

  ns    = { n };
  times = { t };
  fwhms = parameterGrid (0.15, 0.25);

  fileEnding = createFileName(t, fwhm, n, seedStart1, "fwhm", "", q);

  // simulate FWHM vs d
  #pragma omp parallel for
  for (vector<string>::size_type i = 0; i < models.size(); i++) {
     findDetectionBoundary (models[i], fwhms, times, ns,
                      reps, level, seed, "", fileEnding, "1D", q);
  }
  ////////////////////////////////////////////////////////////////////

  // parameter values for t vs d
  gsl_rng_set(seed, seedStart1);

  n = 500;
  fwhm = 0.2;

  ns = { n };
  times = parameterGridInt (50, 500, 50);
  fwhms = { fwhm };

  fileEnding = createFileName(t, fwhm, n, seedStart1, "t", "", q);

  // simulate t vs d
  #pragma omp parallel for
  for (vector<string>::size_type i = 0; i < models.size(); i++) {
     findDetectionBoundary (models[i], fwhms, times, ns,
                      reps, level, seed, "", fileEnding, "1D", q);
  }
  ////////////////////////////////////////////////////////////////////

  // parameter values for n vs d
  gsl_rng_set(seed, seedStart1);

  t = 500;
  fwhm = 0.2;

  ns = parameterGridInt (50, 500, 50);
  times = { t };
  fwhms = { fwhm };

  fileEnding = createFileName(t, fwhm, n, seedStart1, "n", "", q);

  // simulate n vs d
  #pragma omp parallel for
  for (vector<string>::size_type i = 0; i < models.size(); i++) {
     findDetectionBoundary (models[i], fwhms, times, ns,
                      reps, level, seed, "", fileEnding, "1D", q);
  }
  //////////////////////////////////////////////////////////////////

  gsl_rng_free(seed);

  // beep when finished
  cout << '\a';
}
