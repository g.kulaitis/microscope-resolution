#pragma once

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <filesystem>

#include <cmath>
#include <algorithm>

#include <gsl/gsl_rng.h>

using namespace std;   // so that don't have to write std::cout etc.

////////////////////////////////////////////////////////////////////////////////
/////// forward declarations defined ///////////////////////////////////////////
/////// in simulationxxD.cpp files with xx = 1,2 or 3 //////////////////////////
////////////////////////////////////////////////////////////////////////////////

// 1D
vector<double>
doTest (string, int, int, double, double, int, double, gsl_rng *, double);

pair < vector<double>, vector<double> >
generateData (string, int, int, double, pair < vector<double>, vector<double> >,
              gsl_rng *);

// 2D
vector<double>
doTest (string, int, int, double, double, double, int, double, gsl_rng *, double);

pair < vector<vector<double>>, vector<vector<double>> >
generateData (string, int, int, double,
              pair < vector<vector<double>>, vector<vector<double>> >, gsl_rng *);

// 3D
vector<double>
doTest (string, int, int, double, double, double, double, int, double, gsl_rng *, double);

pair < vector<vector<vector<double>>>, vector<vector<vector<double>>> >
generateData (string, int, int, double,
              pair < vector<vector<vector<double>>>, vector<vector<vector<double>>> >,
              gsl_rng *);

// common form to all dimensions
double
calculateQuantile (string, int, int, pair < vector<double>, vector<double> >, double);

pair<double, double>
getTestStats (string, int, int, double, pair < vector<double>, vector<double> >,
              pair < vector<double>, vector<double> >, double);
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void
findDetectionBoundary (string model, vector<double> fwhms, vector<int> times,
                       vector<int> ns, int reps, double level,
                       gsl_rng* seed, string whichCoordinateFwhm = "first",
                       string fileEnding = "", string dimension = "1D", double q = 0.5) {

   string
   fileDir = filesystem::current_path().string() + "/simulationData" + dimension + "/";

   filesystem::create_directory(fileDir);

   string fileName = fileDir;
   fileName += model + "_" + fileEnding + ".csv";

   cout << fileName << endl;

   ofstream outputFile;
   outputFile.open (fileName);

   string firstLine;

   if (dimension == "1D") {
     firstLine = "\"fwhm\"";
   } else if (dimension == "2D") {
          if (whichCoordinateFwhm == "first" ) { firstLine = "\"fwhm\",\"fwhm2\""; }
     else if (whichCoordinateFwhm == "second") { firstLine = "\"fwhm1\",\"fwhm\""; }
     else if (whichCoordinateFwhm == "both"  ) { firstLine = "\"fwhm\",\"fwhm2\""; }
   } else if (dimension == "3D") {
          if (whichCoordinateFwhm == "first" ) { firstLine = "\"fwhm\",\"fwhm2\",\"fwhm3\""; }
     else if (whichCoordinateFwhm == "second") { firstLine = "\"fwhm1\",\"fwhm\",\"fwhm3\""; }
     else if (whichCoordinateFwhm == "third" ) { firstLine = "\"fwhm1\",\"fwhm2\",\"fwhm\""; }
     else if (whichCoordinateFwhm == "all"   ) { firstLine = "\"fwhm\",\"fwhm2\",\"fwhm3\""; }
   }

   firstLine += ",\"t\",\"n\",\"d\",\"level\",\"reps\",\"error1\",\"error2\"";
   // correct formatting to make the top row to string variables, i.e. get "t", "n", etc.
   outputFile << firstLine << endl;

   pair < vector<vector<double>>, vector<vector<double>> >
   probs;

   cout << "Model: " << model << endl;
   cout << "Current parameter values: " << endl;
   cout << "t = " << times[0] << "." << endl;
   cout << "n = " << ns[0] << "." << endl;
   cout << "FWHM = " << fwhms[0] << "." << endl;
   cout << "Performing the test..." << endl;
   cout << endl;

   // always start at distance equal to lowest fwhm
   double d = fwhms[0];

   for (vector<double>::size_type i = 0; i < fwhms.size(); i++) {
      for (vector<double>::size_type j = 0; j < times.size(); j++) {
         for (vector<double>::size_type k = 0; k < ns.size(); k++) {

            //// if the dimension is lower, then others are unused /////////
            double fwhm1, fwhm2, fwhm3;

            // same for 2D and 3D
            if (whichCoordinateFwhm == "first") {
               fwhm1 = fwhms[i];
               fwhm2 = 0.2;  // just hard-code 0.2 in the unused coordinates
               fwhm3 = 0.2;
            } else if (whichCoordinateFwhm == "second") {
               fwhm1 = 0.2;
               fwhm2 = fwhms[i];
               fwhm3 = 0.2;

            // only in 3D
            } else if (whichCoordinateFwhm == "third") {
                fwhm1 = 0.2;
                fwhm2 = 0.2;
                fwhm3 = fwhms[i];
            } else if (whichCoordinateFwhm == "firstAndSecond") {
                fwhm1 = fwhms[i];
                fwhm2 = fwhms[i];
                fwhm3 = 0.2;
            } else if (whichCoordinateFwhm == "secondAndThird") {
                fwhm1 = 0.2;
                fwhm2 = fwhms[i];
                fwhm3 = fwhms[i];
            } else if (whichCoordinateFwhm == "all") {
                fwhm1 = fwhms[i];
                fwhm2 = fwhms[i];
                fwhm3 = fwhms[i];

            // only in 2D
            } else if (whichCoordinateFwhm == "both") {
               fwhm1 = fwhms[i];
               fwhm2 = fwhms[i];

            // 1D
            } else { fwhm1 = fwhms[i]; }
            //////////////////////////////////////////////////////////////


            int t = times[j];
            int n = ns[k];

            vector<double> checkedDists = {};

            int stepSizeCoeff = -1;

            // measure how many times changed from above/below the upper/lower limit
            // set to zero for each new fwhm
            int    swapped = 0;

            // 90% confidence interval
            double upperBound = 1.05*level;
            double lowerBound = 0.95*level;

            // initialize to be larger than 1 so that the loop below works
            vector<double> output = {3, 3};
            double error2 = output[1];


            // depends on the choice of d
            // d = 0 makes it always true
            bool aboveThreshold = true;


            // just loop until type II error is in the interval
            // [0.95*level, 1.05*level)
            while ( error2 < lowerBound || error2 >= upperBound ) {

               cout << "Model: " << model << endl;
               cout << "Current parameter values: " << endl;
               cout << "t = " << t << "." << endl;
               cout << "n = " << n << "." << endl;
               cout << "FWHM1 = " << fwhm1 << "." << endl;
               if (dimension == "2D" || dimension == "3D") {
                  cout << "FWHM2 = " << fwhm2 << "." << endl; }
               if (dimension == "3D") { cout << "FWHM3 = " << fwhm3 << "." << endl; }
               cout << "d = " << d    << "." << endl;
               cout << "Performing the test..." << endl;

               checkedDists.push_back(d);

               if ( checkedDists.size() > 20 || d > 1.0 || d < 0 ) { break; }

               cout << "Checked dists: ";
               for (auto i: checkedDists)
                  cout << i << ' ';
               cout << endl;

               bool aboveThresholdOld = aboveThreshold;

               if (dimension == "1D") {
                 double sd1 = fwhm1 / ( 2*sqrt(2*log(2)) );

                 output = doTest (model, t, n, d, sd1, reps, level, seed, q);

               } else if (dimension == "2D") {
                 double sd1 = fwhm1 / ( 2*sqrt(2*log(2)) );
                 double sd2 = fwhm2 / ( 2*sqrt(2*log(2)) );

                 output = doTest (model, t, n, d, sd1, sd2, reps, level, seed, q);

               } else {
                 double sd1 = fwhm1 / ( 2*sqrt(2*log(2)) );
                 double sd2 = fwhm2 / ( 2*sqrt(2*log(2)) );
                 double sd3 = fwhm3 / ( 2*sqrt(2*log(2)) );

                 output = doTest (model, t, n, d, sd1, sd2, sd3, reps, level, seed, q);

               }

               error2 = output[1];
               cout << "Type II error: " << error2 << endl << endl;

               stringstream buf;
               string       bufString;

               if (dimension == "1D") {
                 buf << fwhm1 << "," << t     << "," << n    << ","
                     << d     << "," << level << "," << reps << ","
                     << output[0]    << ","   << output[1]   << endl;
               } else if (dimension == "2D") {
                 buf << fwhm1 << "," << fwhm2 << "," << t    << "," << n  << ","
                     << d     << "," << level << "," << reps << ","
                     << output[0]    << ","   << output[1]   << endl;
               } else {
                 buf << fwhm1 << "," << fwhm2 << "," << fwhm3 << "," << t    << ","
                     << n     << "," << d     << "," << level << "," << reps << ","
                     << output[0]    << ","   << output[1]    << endl;
               }

               bufString = buf.str();

               // write each line separately so that don't have to wait until the code is finished
               outputFile << bufString;

               #pragma omp critical
               outputFile.flush();

               if ( error2 > upperBound ) { aboveThreshold = true;  }
               else                       { aboveThreshold = false; }

               // decrease step size if changed from too low to too high
               if ( aboveThresholdOld != aboveThreshold ) { stepSizeCoeff += 1; }

               // Make same check twice to see if the distance should be changed.
               // If the code advances to the next larger FWHM, then d should stay
               // the same or increase.
               if ( error2 < lowerBound || error2 > upperBound ) {

                  if ( aboveThreshold ) { d += 0.01/pow(2, stepSizeCoeff); }
                  else                  { d -= 0.01/pow(2, stepSizeCoeff); }

                  // check if the distance has been already checked
                  if ( find(checkedDists.begin(), checkedDists.end(), d) != checkedDists.end() ) {

                     stepSizeCoeff += 1; // decrease the step size

                     // go back to previous distance and take half the previous step
                     if ( aboveThreshold ) {
                        d -= 0.01/pow(2, stepSizeCoeff-1);
                        d += 0.01/pow(2, stepSizeCoeff  );
                     } else {
                        d += 0.01/pow(2, stepSizeCoeff-1);
                        d -= 0.01/pow(2, stepSizeCoeff  );
                        }
                  }
               } //else { d += 0.005; } // advance a bit for the next parameter value
            }
         }
      }
   }
   outputFile.close();
}

vector<double>
parameterGrid (double parStart, double parEnd) {
   double par = parStart;
   vector<double> pars;

   while (par < parEnd + 0.0001*parEnd) {
      pars.push_back(par);
      par += 0.01;
   }

   return pars;
}

vector<int>
parameterGridInt (int parStart, int parEnd, int stepSize) {
   int par = parStart;
   vector<int> pars;

   while (par < parEnd + 0.0001*parEnd) {
      pars.push_back(par);
      par += stepSize;
   }

   return pars;
}

string
removeZeroes (string str) {

   // count trailing zeros
   int i = 0;
   while (str[str.size() - 1] == '0') {
       ++i;
       str.pop_back();
   }
   return str;
}

string
createFileName (int t = 10, double fwhm = 0.2, int n = 10, int seed = 1,
                  string variable = "t", string whichCoordinateFwhm = "first", double q = 0.5) {
   string fileName;
   string parStr;

   if (variable == "fwhm") {
      fileName = "fwhm_t=" + to_string(t) + "_n=" + to_string(n)
                  + "_seed=" + to_string(seed);

      if (whichCoordinateFwhm != "") { fileName += "_fwhm=" + whichCoordinateFwhm; }
   }
   else if (variable == "t") {
      parStr = removeZeroes(to_string(100*fwhm));

      fileName = "t_fwhm=" + parStr.substr(0, 2) + "_n=" + to_string(n)
                  + "_seed=" + to_string(seed);
   }
   else if (variable == "n") {
      parStr = removeZeroes(to_string(100*fwhm));

      fileName = "n_t=" + to_string(t) + "_fwhm=" + parStr.substr(0, 2)
                  + "_seed=" + to_string(seed);
   }

   parStr = removeZeroes(to_string(1000*q));
   fileName += "_q=" + parStr.substr(0, 3);

   return fileName;
}
